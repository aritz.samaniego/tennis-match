package org.match.tennis;

import java.util.List;

import org.match.tennis.function.MatchRule;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@AllArgsConstructor
@Builder(toBuilder=true)
@Getter
public class Score {
	
	public enum Point {
		
		Zero("0",0), Fifteen("15",1), Thirty("30",2), Fourty("40",3), Win("Win",4), Advantage("A",5), WinWithAdvantage("Win With Advantage",5);
	
		private String value;
		int index;
		
		Point(String value, Integer index) {
			this.value = value;
			this.index = index;
		}
		
		Point valueOf(Integer index) {
			switch(index) {
			case 0: return Zero;
			case 1: return Fifteen;
			case 2: return Thirty;
			case 3: return Fourty;
			case 4: return Win;
			}
			return null;
		}
		
		Point next() {
			if (Advantage.index == this.index) {
				return WinWithAdvantage;
			}
			return valueOf(this.index + 1);
		}
		
		public String value() {
			return this.value;
		}
	}
	
	public enum Player {Server, Receiver};

	@Builder.Default
	private Point server = Point.Zero;
	@Builder.Default
	private Point receiver = Point.Zero;
	
	private Point lastPoint;
	private Player lastPointPlayer;
	private Player winner;
	
	public Score applyRules(Score currentScore, List<MatchRule> rules) {
		Score score = currentScore;
		for(MatchRule rule : rules) {
			score = rule.apply(score);
		}
		return score;
	}
	
	public String print() {
		return server.value + ":" + receiver.value;
	}
	
	public Point getScore(Player player) {
		switch (player) {
		case Server: return server;
		case Receiver: return receiver;
		}
		return null;
	}
	
	public int getScoreDifference() {
		return Math.abs(this.receiver.index - this.server.index);
	}

	public ScoreBuilder builderClone() {
		return this.toBuilder();
	}
}
