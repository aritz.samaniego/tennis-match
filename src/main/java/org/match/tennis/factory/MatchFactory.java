package org.match.tennis.factory;

import java.util.List;

import org.match.tennis.Match;
import org.match.tennis.function.MatchRule;

public class MatchFactory {

	public static Match create() {
		return Match.builder().rules(Match.DEFAULT_RULES).build();
	}
	
	public static Match create(List<MatchRule> matchRules) {
		return Match.builder().rules(matchRules).build();
	}
}
