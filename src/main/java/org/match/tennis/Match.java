package org.match.tennis;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.match.tennis.Score.Player;
import org.match.tennis.Score.Point;
import org.match.tennis.exception.MatchIsFinishedException;
import org.match.tennis.function.MatchRule;
import org.match.tennis.rule.AdvantageRule;
import org.match.tennis.rule.DeuceRule;
import org.match.tennis.rule.WinRule;

import lombok.Builder;

@Builder
public class Match {
	
	public static final List<MatchRule> DEFAULT_RULES = Arrays.asList(new AdvantageRule(), new DeuceRule(), new WinRule()); 

	@Builder.Default
	private List<MatchRule> rules = Collections.emptyList();

	public Score start() {
		return Score.builder().build();
	}
	
	public Score score(Score currentScore, Player player) throws MatchIsFinishedException {
		if (Optional.ofNullable(currentScore.getWinner()).isPresent()) {
			throw new MatchIsFinishedException();
		}
		Score.ScoreBuilder scoreBuilder = currentScore.builderClone();
		Point currentPoint = currentScore.getScore(player);
		Point newPoint = currentPoint.next();
		
		switch (player) {
		case Server:
			scoreBuilder.server(newPoint);
			break;
		case Receiver:
			scoreBuilder.receiver(newPoint);
			break;
		default:
			break;
		}
		
		scoreBuilder.lastPoint(newPoint);
		scoreBuilder.lastPointPlayer(player);
		
		Score score = scoreBuilder.build();
		score = score.applyRules(score, rules);
		return score;
	}
}

