package org.match.tennis.function;

import org.match.tennis.Score;

@FunctionalInterface
public interface MatchRule {

	Score apply(Score score);
}
