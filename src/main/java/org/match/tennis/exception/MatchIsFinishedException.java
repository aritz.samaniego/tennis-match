package org.match.tennis.exception;

public class MatchIsFinishedException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public MatchIsFinishedException() {
        super("This match has finished. Try starting a new one.");
    }
	
}
