package org.match.tennis.rule;

import org.match.tennis.Score;
import org.match.tennis.Score.Point;
import org.match.tennis.function.MatchRule;

public class AdvantageRule implements MatchRule {

	@Override
	public Score apply(Score score) {
		Score.ScoreBuilder scoreBuilder = score.builderClone();
		if (Point.Win.equals(score.getLastPoint()) && (score.getScoreDifference() < 2)) {
			switch (score.getLastPointPlayer()) {
			case Server:
				scoreBuilder.server(Point.Advantage);
				break;
			case Receiver:
				scoreBuilder.receiver(Point.Advantage);
				break;
			}
		}
		return scoreBuilder.build();
	}

}
