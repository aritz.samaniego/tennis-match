package org.match.tennis.rule;

import org.match.tennis.Score;
import org.match.tennis.Score.Point;
import org.match.tennis.function.MatchRule;

public class DeuceRule implements MatchRule {

	@Override
	public Score apply(Score score) {
		Score.ScoreBuilder scoreBuilder = score.builderClone();
		if (Point.Advantage.equals(score.getServer()) && Point.Advantage.equals(score.getReceiver())) {
			scoreBuilder.server(Point.Fourty);
			scoreBuilder.receiver(Point.Fourty);
		}
		return scoreBuilder.build();
	}

}
