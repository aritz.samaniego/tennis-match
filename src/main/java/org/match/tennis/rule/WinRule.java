package org.match.tennis.rule;

import org.match.tennis.Score;
import org.match.tennis.Score.Player;
import org.match.tennis.Score.Point;
import org.match.tennis.function.MatchRule;

public class WinRule implements MatchRule {

	@Override
	public Score apply(Score score) {
		Score.ScoreBuilder scoreBuilder = score.builderClone();
		if (score.getScoreDifference() == 2) {
			if (Point.Win.equals(score.getServer()) || Point.WinWithAdvantage.equals(score.getServer())) {
				scoreBuilder.winner(Player.Server);
			} else if (Point.Win.equals(score.getReceiver()) || Point.WinWithAdvantage.equals(score.getReceiver())) {
				scoreBuilder.winner(Player.Receiver);
			}
		}
		return scoreBuilder.build();
	}

}
