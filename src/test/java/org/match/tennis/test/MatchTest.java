package org.match.tennis.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Collections;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.match.tennis.Match;
import org.match.tennis.Score;
import org.match.tennis.Score.Player;
import org.match.tennis.Score.Point;
import org.match.tennis.exception.MatchIsFinishedException;
import org.match.tennis.factory.MatchFactory;
import org.match.tennis.function.MatchRule;

public class MatchTest {
	
	private Match match;
	private Score initialScore;
	
	@BeforeEach
	public void initMatch() {
		match = MatchFactory.create();
		initialScore = match.start();
	}

	@DisplayName("Match Starts Correctly")
	@Test
	public void matchStarter() {
		assertEquals(Point.Zero, initialScore.getServer());
		assertEquals(Point.Zero, initialScore.getReceiver());
		assertEquals("0:0", initialScore.print());
	}
	
	/**
	 * 	As a library user
		I want the score to increase when a player wins a point
		So that I can display the current score correctly
	 * 
	 */
	@DisplayName("Winning a Point Increases Score Correctly")
	@Test
	public void winningaPointIncreasesScoreCorrectlyTest() throws MatchIsFinishedException {
		// Given the score is 0:0
		Score initialScore = Score.builder().server(Point.Zero).receiver(Point.Zero).build();
		//When the server wins a point
		Score score = this.match.score(initialScore, Player.Server);
		// Then the score is 15:0
		assertEquals("15", score.getServer().value());
		assertEquals("0", score.getReceiver().value());
		assertEquals("15:0", score.print());

		// Given the score is 15:15
		score = Score.builder().server(Point.Fifteen).receiver(Point.Fifteen).build();
		// When the receiver wins a point
		score = this.match.score(score, Player.Receiver);
		// Then the score is 15:30
		assertEquals("15", score.getServer().value());
		assertEquals("30", score.getReceiver().value());
		assertEquals("15:30", score.print());

		// Given the score is 30:30
		score = Score.builder().server(Point.Thirty).receiver(Point.Thirty).build();
		// When the server wins a point
		score = this.match.score(score, Player.Server);
		// Then the score is 40:30
		assertEquals("40", score.getServer().value());
		assertEquals("30", score.getReceiver().value());
		assertEquals("40:30", score.print());
	}
	
	/**
	 * 	As a library user
		I want deuce and advantage to be scored correctly
		So that I can display the current score correctly
	 *  
	 */
	@DisplayName("Deuce and Advantage are Scored Correctly")
	@Test
	public void deuceAndAdvantageAreScoredCorrectlyTest() throws MatchIsFinishedException {
		// Given the score is 40:40
		Score score = Score.builder().server(Point.Fourty).receiver(Point.Fourty).build();
		// When the receiver wins a point
		score = this.match.score(score, Player.Receiver);
		// Then the score should be 40:A
		assertEquals("40", score.getServer().value());
		assertEquals("A", score.getReceiver().value());
		assertEquals("40:A", score.print());

		// Given the score is A:40
		score = Score.builder().server(Point.Advantage).receiver(Point.Fourty).build();
		// When the receiver wins a point
		score = this.match.score(score, Player.Receiver);
		// Then the score should be 40:40
		assertEquals("40", score.getServer().value());
		assertEquals("40", score.getReceiver().value());
		assertEquals("40:40", score.print());
	}
	
	/**
	 * 	As a library user
		I want the winning point to be scored correctly
		So that I can display the winner
	 * 
	 */
	@DisplayName("Winning Points are Scored Correctly")
	@Test
	public void winningPointsAreScoredCorrectlyTest() throws MatchIsFinishedException {
		// Given the score is 40:30
		Score score = Score.builder().server(Point.Fourty).receiver(Point.Thirty).build();
		// When the server wins a point
		score = this.match.score(score, Player.Server);
		// Then the server should win
		assertEquals(Player.Server, score.getWinner());
		
		// Given the score is 40:A
		score = Score.builder().server(Point.Fourty).receiver(Point.Advantage).build();
		// When the receiver wins a point
		score = this.match.score(score, Player.Receiver);
		// Then the receiver should win
		assertEquals(Player.Receiver, score.getWinner());
	}
	
	/**
	 * 	As a library user with my custom ruleset loaded
		I want the winning point to be scored correctly
		So that I can display the winner
	 * 
	 */
	@DisplayName("External rule loading")
	@Test
	public void externalRuleLoadingTest() throws MatchIsFinishedException {
		MatchRule rule = score -> {
			Score.ScoreBuilder scoreBuilder = score.builderClone();
			if (Point.Win.equals(score.getServer())) {
				scoreBuilder.winner(Player.Server);
			} else if (Point.Win.equals(score.getReceiver())) {
				scoreBuilder.winner(Player.Receiver);
			}
			return scoreBuilder.build();
		};
		this.match = MatchFactory.create(Collections.singletonList(rule));
		// Given the score is 40:40
		Score score = Score.builder().server(Point.Fourty).receiver(Point.Fourty).build();
		// When the server wins a point
		score = this.match.score(score, Player.Server);
		// Then the server should win
		assertEquals(Player.Server, score.getWinner());

		// Given the score is 40:40
		score = Score.builder().server(Point.Fourty).receiver(Point.Fourty).build();
		// When the receiver wins a point
		score = this.match.score(score, Player.Receiver);
		// Then the receiver should win
		assertEquals(Player.Receiver, score.getWinner());
	}
	
	@DisplayName("Match Finish Exception")
	@Test
	public void matchFinishExceptionTest() {
		Score score = Score.builder().winner(Player.Server).build();
		assertThrows(MatchIsFinishedException.class, () -> this.match.score(score, Player.Server));
		assertThrows(MatchIsFinishedException.class, () -> this.match.score(score, Player.Receiver));

	}
}
