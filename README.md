#Introduction#

The purpose of this library is to give the basic functionality for scoring points on a single game of tennis.

#Usage#

Using <code>MatchFactory</code> a new match is created.

<code>
  
	Match match = MatchFactory.create();
  	
</code> 

In order to start the match <code>start()</code> method must be used which will initialize the score to 0:0. This method will return the current score result:

<code>

	Score currentScore = match.start();

</code>

Scoring points are counted by <code>score()</code> method passing currentScore and the player scoring the new point. For each scoring a new instance of <code>Score</code> is created will the refreshed values:

<code>

	Score currentScore = match.score(currentScore, Player.Server);
	
</code>

When a player has won the match after scoring a point <b>winner</b> property will be informed and subsequent tries of scoring point will throw a <code>MatchIsFinishedException</code>.

#Custom rule loading#

When a new match is created using <code>MatchFactory.create()</code> default match rules are applied for each scoring which are:

- Advantage rule
- Deuce rule
- Win rule

Match rules are <code>MatchRule</code> functional interface implementations.

If custom rules are desired they must be passed to the factory's create() method:

<code>

	MatchRule singlePointAdvantageRule = score -> {
			Score.ScoreBuilder scoreBuilder = score.builderClone();
			if (Point.Win.equals(score.getServer())) {
				scoreBuilder.winner(Player.Server);
			} else if (Point.Win.equals(score.getReceiver())) {
				scoreBuilder.winner(Player.Receiver);
			}
			return scoreBuilder.build();
		};
	List<MatchRule> customRules = Arrays.asList(singlePointAdvantageRule);
	Match match = MatchFactory.create(customRules);
</code>

<b>It must be noticed that using custom rules will remove all the default ones</b>, so if any of those is needed it must be passed as custom rule as well.